open Libcbv
open Syntax
open Terms
open Semantics.AbstractMachine

let run fmt printer t = Printf.printf fmt (IO.document_to_string printer t)
let run_printer fmt t = run fmt Printer.print_term t
let run_value_of fmt t = run fmt Printer.print_value (value_of t)
(* let run_cps_of fmt t = run fmt Printer.pretty_term (cps_of t) *)

let () =
  run_printer "variable : %s \n" tx;
  run_printer "unit : %s \n" tunit;
  run_printer "function : %s \n" tfun_id;
  run_printer "application : %s \n" tapp_id_id;
  run_printer "application on unit: %s \n" tapp_id_unit;
  run_value_of "value of unit : %s\n" tunit;
  run_value_of "value of  (fun x -> x) (fun x -> x): %s \n" tapp_id_id;
  run_value_of "value of  (fun x -> x) (): %s \n" tapp_id_unit
(* run_cps_of "A CPS translation variable : %s \n" tx;
 * run_cps_of "A CPS translation unit : %s \n" tunit;
 * run_cps_of "A CPS translation function : %s \n" tfun_id;
 * run_cps_of "A CPS translation application : %s \n" tapp_id_id;
 * run_cps_of "A CPS translation application on unit: %s \n" tapp_id_unit;
 * run_cps_of "A CPS translation of  (fun x -> x) (fun y -> y) (): %s \n"
     tapp_id_idy_unit *)
