open Libcbv.Typechecker
open ANFConversion
open ANFLambdaLifting
open NamedANFToDeBruijn
open ANFUncurry
open ANFToMimi
module Mimi = Libmimi

let normal_form p =
  elaborate p |> to_anf |> encode_variables |> uncurry |> lambda_lift

let normal_form' p =
  elaborate p |> to_anf |> encode_variables |> lambda_lift |> uncurry

let translate p = compile (normal_form p)
let mimi_typcheck p = translate p |> Mimi.Typechecker.(elaborate base)

let mimi_eval p =
  let open Mimi.Typechecker in
  mimi_typcheck p |> function
  | J { kinstr; _ } ->
      let code = kinstr IHalt in
      Mimi.Semantics.CStackMachine.(step (Obj.magic code) empty_stack)

let string_of_value =
  let open Libcbv in
  IO.document_to_string Libcbv.Printer.print_value

let print_src_tgt p =
  let anf = normal_form p |> ANFToMimi.string_of_anf in
  let mimi = translate p |> ANFToMimi.string_of_mimi in
  Printf.printf "ANF of:\n---\n%s\n     -->\n\n---\n%s\n" anf mimi

let eval_debruijn_anf = Libanf.Semantics.DeBruijnBigStep.value_of

let _print_value_of_anf p =
  try
    let nf = normal_form p in
    let v =
      eval_debruijn_anf nf
      |> Libcbv.IO.document_to_string Libanf.Printer.print_value
    in
    Printf.printf "Value of ANF:\n---\n%s \n     -->\n\n---\n%s\n\n"
      (ANFToMimi.string_of_anf nf)
      v
  with ANFConversion.UntypedTerm t ->
    Printf.printf "Untyped Term : %s\n"
      Libcbv.(IO.document_to_string Printer.print_term t)

let print_value_of_src p =
  (* let nf = nf p in *)
  let v = Libcbv__Semantics.AbstractMachine.value_of p |> string_of_value in
  Printf.printf "Value of:\n---\n%s\n\n-->\n\n---\n%s\n\n"
    Libcbv.(IO.document_to_string Printer.print_term (elaborate p))
    v

let%test _ =
  try mimi_eval Libcbv.Syntax.Terms.s = (42, ((), ()))
  with Mimi.Typechecker.TypeError msg ->
    print_endline (Lazy.force msg);
    false

let%test _ = mimi_eval Libcbv.Syntax.Terms.p = (42, ((), ()))

let one_two_three =
  Libcbv.Syntax.Terms.(List [ Lit (Int 1); Lit (Int 2); Lit (Int 3) ])

let%test _ = mimi_eval one_two_three = ([ 1; 2; 3 ], ((), ()))

let () =
  _print_value_of_anf
    Libcbv.Syntax.Terms.(
      List
        [ Lit (Int 1); Lit (Int 2); BinArith (Lit (Int 1), Add, Lit (Int 2)) ])

let prog =
  "let (nil_int:int list) = [] in\n\
  \   let one_t (u:unit) :int = 1 in\n\
  \   let step\n\
  \     (s: (int list) * (unit -> int)) : ((int list) * (unit -> int)) =\n\
  \      let (h : int list) = fst s in\n\
  \      let (b : unit -> int) = snd s in\n\
  \       let (v:int) = b () in (v::h,b) in\n\
  \   let (p: (int list ) *(unit -> int)) = step (nil_int, one_t) in\n\
  \   let (p: (int list ) *(unit -> int)) = step p in\n\
  \   let (p: (int list ) *(unit -> int)) = step p in\n\
  \   p"

(* let rec x = 1::x *)

let id = "let (id : unit -> int -> int) = fun (u:unit) (x:int) -> x in id () 42"
let () = print_src_tgt (Libcbv.IO.parse_program id)
let () = print_value_of_src (Libcbv.IO.parse_program id)

let fact_5 =
  "let  rec fact (x:int) : int = if is_zero x then 1 else x *  fact (x - 1 ) \
   in fact 5"

let fact_unit_5 =
  "let  rec (fact: unit -> (int -> int)) =\n\
  \  fun (u:unit) ->\n\
  \  fun (x:int) -> if is_zero x then 1 else x *  fact () (x - 1 ) in fact () 5"

let stream_one =
  "let rec st_one (u:unit) : (int * (unit -> (int * (unit -> ?) = (1, st_one)"

let () = print_src_tgt (Libcbv.IO.parse_program fact_unit_5)
let () = print_value_of_src (Libcbv.IO.parse_program fact_5)

let%test _ =
  try Libcbv.IO.parse_program fact_unit_5 |> mimi_eval = (120, ((), ()))
  with Libmimi.Typechecker.TypeError m ->
    print_endline (Lazy.force m);
    flush_all ();
    false

let () = Libcbv.IO.parse_program fact_5 |> print_src_tgt

let%test _ = Libcbv.IO.parse_program id |> mimi_eval = (42, ((), ()))

let%test _ =
  let res = Libcbv.IO.parse_program prog |> mimi_eval in
  fst (fst res) = [ 1; 1; 1 ]

(* let () =
 *   let step s =
 *     let h = fst s in
 *     let b = snd s in
 *     (b()::h,b)
 *   in
 *   let one = ([], fun () -> 1) *)

(* let () = _print_value_of_anf Libcbv.Syntax.Terms.p
 * let () = print_value_of_src Libcbv.Syntax.Terms.p
 * let () = print_src_tgt Libcbv.Syntax.Terms.p
 * let () = print_src_tgt Libcbv.Syntax.Terms.s *)
