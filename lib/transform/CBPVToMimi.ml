open CBPVToCBV
open CBVToMimi
module S = Libcbpv.Syntax.Terms
module STypes = Libcbpv.Syntax.Types

let compile (p : STypes.tycomp S.term) = cbpv_comp_to_cbv p |> translate

let id =
  let open S in
  tlet "id"
    (ty_thunk (ty_fun ty_int ty_int))
    (tthunk (tfun "x" ty_int (treturn (t "x"))))
    (treturn (t "id"))

let return_one =
  let open S in
  tlet "one" ty_int (tint 1)
    (tlet "a"
       (ty_thunk (ty_fun ty_int ty_int))
       (tthunk (tfun "x" ty_int (treturn (t "one"))))
       (treturn (t "a")))

let fact n =
  let open S in
  tlet_rec "fact" (ty_fun ty_int ty_int)
    (tfun "x" ty_int
       ((Prim (CompNZ (t "x")) =: ("zx", ty_bool))
          (tif (t "zx")
             (treturn (tint 1))
             ((t "x" -! tint 1 =: ("predx", ty_int))
                ((t "predx" |>! !!(t "fact") =: ("predf", ty_int))
                   (t "x" *! t "predf"))))))
    (tint n |>! !!(t "fact"))

let print name p =
  Printf.printf "mimi of cbpv %s:\n%s\n" name
    (ANFToMimi.string_of_anf (cbpv_comp_to_cbv p |> normal_form))

(* let () = print "id" id
 * let () = print "return_one " return_one *)
let () = print "fact 5" (fact 5)

let%test _ =
  try mimi_eval (cbpv_comp_to_cbv (fact 5)) = (120, ((), ()))
  with Libmimi.Typechecker.TypeError m ->
    print_endline (Lazy.force m);
    false
