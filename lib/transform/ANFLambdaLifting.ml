(* Source Language *)
module S = Libanf.Syntax.DeBruijnTerms

(* Target Language *)
module T = Libanf.Syntax.DeBruijnTerms
open Libanf.Syntax.Types

let empty = []
let bind env x = List.cons x env
let get env x = List.nth env x

let _free_vars n a ty_env =
  S.fold_computation n ty_env
    (fun depth _tys acc a ->
      match a with
      | S.(Var i) when succ i > depth -> (a, depth, List.nth _tys i) :: acc
      | _ -> acc)
    [] a
  |> List.rev

let free_vars a ty_env = _free_vars 0 a ty_env
let free_vars_rec a ty_env = _free_vars 1 a ty_env

let lift_var k d a =
  match a with
  | S.(Atom (Var i)) when succ i > d -> S.(Atom (Var (i + k)))
  | _ -> a

let lift_body k = S.map_body 0 (lift_var k)
let lift_comp k = S.map_computation 0 (lift_var k)
let lift_atom k = S.map_atom 0 (lift_var k)

let rec _lambda_lift env t =
  match t with
  | S.(Let (ty, Lam (ar, var, covar, lb), b)) ->
      let env' = List.rev var @ env in
      let lb = _lambda_lift env' lb in
      let lam = T.Lam (ar, var, covar, lb) in
      let fv = free_vars lam env in
      if fv = [] then T.Let (ty, lam, _lambda_lift (bind env ty) b)
      else
        T.(
          Let
            ( ty,
              lift_lambda env' ar var covar lb fv,
              _lambda_lift (bind env ty) b ))
  | S.(LetRec (ty, Lam (ar, var, covar, lb), b)) ->
      let env' = List.rev var @ (ty :: env) in
      let lb = _lambda_lift env' lb in
      let lam = T.Lam (ar, var, covar, lb) in
      let fv = free_vars_rec lam (ty :: env) in
      if fv = [] then T.LetRec (ty, lam, _lambda_lift (bind env ty) b)
      else
        T.(
          LetRec
            ( ty,
              lift_lambda env' ar var covar lb fv,
              _lambda_lift (bind env ty) b ))
  | S.LetRec _ -> failwith "Recursived values are un supported"
  | S.Let (ty, c, b) -> T.Let (ty, c, _lambda_lift (bind env ty) b)
  | S.(Computation (Lam (ar, var, covar, b))) ->
      let env' = List.rev var @ env in
      let b = _lambda_lift env' b in
      let lam = T.Lam (ar, var, covar, b) in
      let fv = free_vars lam env in
      if fv = [] then T.Computation lam
      else T.(Computation (lift_lambda env ar var covar b fv))
  | S.Computation c -> T.Computation c

and get_var_index = function S.Var i -> i | _ -> assert false

and lift_lambda _env ar var covar b fvs : T.computation =
  let ar' = ar + List.length fvs in
  let var_map =
    List.mapi (fun pos (fv, depth, _) -> (fv, S.Var (depth + pos))) fvs
  in
  let rename v =
    match v with
    | S.Atom (Var _ as v) -> (
        try S.Atom (List.assoc v var_map) with Not_found -> S.Atom v)
    | _ -> v
  in
  let ty' =
    List.map (function S.Var _, _, ty -> ty | _ -> assert false) fvs @ var
  in

  let b' = S.map_body 0 (fun _ v -> rename v) b in
  PartialApp
    ( Lam (ar', ty', covar, b'),
      List.map (fun (v, d, _) -> T.Var (get_var_index v - d)) fvs )

let a =
  let open S in
  clet int_ty
    (tval (tint 42))
    (tcomp (Lam (3, [ int_ty; int_ty; int_ty ], int_ty, tcomp (tval (tvar 3)))))

let b =
  let open S in
  clet int_ty
    (tval (tint 28))
    (clet int_ty
       (tval (tint 42))
       (tcomp
          (Lam (3, [ int_ty; int_ty; int_ty ], int_ty, tcomp (tval (tvar 4))))))

let c =
  let open S in
  clet int_ty
    (tval (tint 42))
    (clet (arrow_ty int_ty int_ty)
       (tlam 1 int_ty int_ty (tcomp (tval (tvar 1))))
       (tcomp (Pair (tvar 0, tvar 0))))

let lambda_lift = _lambda_lift T.Env.empty

let%test _ =
  let open T in
  lambda_lift a
  = clet int_ty (tval (Lit (Int 42)))
      (tcomp
         (PartialApp
            ( Lam
                ( 4,
                  [ int_ty; int_ty; int_ty; int_ty ],
                  int_ty,
                  tcomp (tval (tvar 3)) ),
              [ tvar 0 ] )))

let%test _ =
  let open T in
  lambda_lift b
  = clet int_ty (tval (Lit (Int 28)))
      (clet int_ty (tval (Lit (Int 42)))
         (tcomp
            (PartialApp
               ( Lam
                   ( 4,
                     [ int_ty; int_ty; int_ty; int_ty ],
                     int_ty,
                     tcomp (tval (tvar 3)) ),
                 [ tvar 1 ] ))))

let lifted_c =
  let open T in
  clet int_ty
    (tval (tint 42))
    (clet (arrow_ty int_ty int_ty)
       (PartialApp
          ( Lam (2, [ int_ty; int_ty ], int_ty, tcomp (tval (tvar 1))),
            [ tvar 0 ] ))
       (tcomp (Pair (tvar 0, tvar 0))))

let%test _ = lambda_lift c = lifted_c

let d =
  let open T in
  clet
    (arrow_ty (TyProd (TyProd (int_ty, int_ty), arrow_ty int_ty int_ty)) int_ty)
    (Lam
       ( 2,
         [ TyProd (int_ty, int_ty); arrow_ty int_ty int_ty ],
         int_ty,
         clet (arrow_ty int_ty int_ty)
           (tlam 1 int_ty int_ty
              (clet int_ty
                 (Snd (tvar 2))
                 (clet int_ty
                    (tval (tvar 1))
                    (clet int_ty
                       (tapp (tvar 3) (tvar 2))
                       (tcomp (BinArith (tvar 0, Add, tvar 1)))))))
           (tcomp (tapp (tvar 0) (tint 0))) ))
    (tcomp (tval (tvar 0)))

let%test _ =
  let open T in
  lambda_lift d
  = Let
      ( TyArrow
          ([ TyProd (TyProd (TyInt, TyInt), TyArrow ([ TyInt ], TyInt)) ], TyInt),
        Lam
          ( 2,
            [ TyProd (TyInt, TyInt); TyArrow ([ TyInt ], TyInt) ],
            int_ty,
            Let
              ( TyArrow ([ TyInt ], TyInt),
                PartialApp
                  ( Lam
                      ( 3,
                        [
                          TyProd (TyInt, TyInt);
                          TyArrow ([ TyInt ], TyInt);
                          TyInt;
                        ],
                        int_ty,
                        Let
                          ( TyInt,
                            Snd (Var 1),
                            Let
                              ( TyInt,
                                Val (Var 1),
                                Let
                                  ( TyInt,
                                    App (Var 4, Var 1),
                                    Computation (BinArith (Var 0, Add, Var 1))
                                  ) ) ) ),
                    [ Var 1; Var 0 ] ),
                Computation (App (Var 0, Lit (Int 0))) ) ),
        Computation (Val (Var 0)) )
