(* Source Language *)
module S = Libanf.Syntax.DeBruijnTerms

(* Target Language *)
module T = Libanf.Syntax.DeBruijnTerms
open Libanf.Syntax.Types

let rec lambda_bindings_body lambda_bound_variables tys f acc b =
  match b with
  | T.Let (_, c, b) ->
      let acc =
        lambda_bindings_computation lambda_bound_variables tys f acc c
      in
      lambda_bindings_body lambda_bound_variables tys f acc b
  | T.LetRec (_, c, b) ->
      let acc =
        lambda_bindings_computation lambda_bound_variables tys f acc c
      in
      lambda_bindings_body lambda_bound_variables tys f acc b
  | T.Computation c ->
      lambda_bindings_computation lambda_bound_variables tys f acc c

and lambda_bindings_computation lambda_bound_variables var f acc c =
  match c with
  | T.Val a -> lambda_bindings_atom lambda_bound_variables var f acc a
  | T.Lam (arity, var', _, b) ->
      lambda_bindings_body (lambda_bound_variables + arity) (var @ var') f acc b
  | T.BinArith (a1, _, a2) | T.App (a1, a2) ->
      let acc = lambda_bindings_atom lambda_bound_variables var f acc a1 in
      lambda_bindings_atom lambda_bound_variables var f acc a2
  | T.If (c, bt, bf) ->
      let acc = lambda_bindings_atom lambda_bound_variables var f acc c in
      let acc = lambda_bindings_body lambda_bound_variables var f acc bt in
      lambda_bindings_body lambda_bound_variables var f acc bf
  | T.PartialApp (a, xs) ->
      let acc =
        lambda_bindings_computation lambda_bound_variables var f acc a
      in
      List.fold_left
        (fun acc a -> lambda_bindings_atom lambda_bound_variables var f acc a)
        acc xs
  | T.Fst a | T.Snd a | T.CompNZ a ->
      lambda_bindings_atom lambda_bound_variables var f acc a
  | T.Pair (a, b) ->
      let acc = lambda_bindings_atom lambda_bound_variables var f acc a in
      lambda_bindings_atom lambda_bound_variables var f acc b
  | T.Cons (a, b) ->
      let acc = lambda_bindings_atom lambda_bound_variables var f acc a in
      lambda_bindings_atom lambda_bound_variables var f acc b
  | T.List (_, xs) ->
      List.fold_left (lambda_bindings_atom lambda_bound_variables var f) acc xs

and lambda_bindings_atom lambda_bound_variables tys f acc a =
  f lambda_bound_variables tys acc a

let rec _uncurry env t =
  match t with
  | S.Let (ty, c, b) ->
      let ty, c =
        match (ty, uncurry_comp env false c) with
        | _, (T.(Lam (_, var, covar, _)) as c) ->
            let covar = coerce covar in
            (TyArrow (var, covar), c)
        | ty, comp -> (uncurry_type ty, comp)
      in
      let b = _uncurry (T.Env.bind env ty) b in
      T.Let (ty, c, b)
  | S.LetRec (ty, Lam (base_arity, var, covar, lb), b) ->
      let ty, c =
        match (ty, uncurry_lambda env true base_arity var covar lb) with
        | _, (T.(Lam (_, var, covar, _)) as c) ->
            let covar = coerce covar in
            (TyArrow (var, covar), c)
        | _ -> assert false
      in
      let b = _uncurry (T.Env.bind env ty) b in
      T.LetRec (ty, c, b)
  | S.Computation c -> T.Computation (uncurry_comp env false c)
  | _ -> failwith "Only lambdas can be recursive"

and coerce ty =
  match ty with
  | TyArrow (_, ret) -> coerce ret
  | TyProd (ty1, ty2) -> TyProd (uncurry_type ty1, uncurry_type ty2)
  | _ -> uncurry_type ty

and uncurry_type ty =
  match ty with
  | TyArrow (tys1, TyArrow (tys2, ret)) ->
      uncurry_type (TyArrow (tys1 @ tys2, ret))
  | TyArrow (tys1, tys2) -> TyArrow (tys1, uncurry_type tys2)
  | TyProd (ty1, ty2) -> TyProd (uncurry_type ty1, uncurry_type ty2)
  | TyList ty -> TyList (uncurry_type ty)
  | _ -> ty

and uncurry_comp env is_rec c =
  match c with
  | S.Lam (base_arity, var, covar, b) ->
      uncurry_lambda env is_rec base_arity var covar b
  | S.App ((Var i as v), b) ->
      if arity_by_type env i = 1 then T.App (v, b)
      else T.PartialApp (T.Val v, [ b ])
  | S.PartialApp (a, ps) -> T.PartialApp (uncurry_comp env is_rec a, ps)
  | S.If (c, bt, bf) -> T.If (c, _uncurry env bt, _uncurry env bf)
  | S.Val _ | S.BinArith _ | S.Snd _ | S.Fst _ | S.CompNZ _ | S.Pair _
  | S.List _ | S.Cons _ ->
      c
  | _ -> assert false
(* by typing *)

(* and _arity_of_lambda t = match t with Lam (ar, _, _) -> ar | _ -> 0 *)

and arity_by_type env var =
  match T.Env.get env var with TyArrow (args, _) -> List.length args | _ -> 0

and get_lambda_bindings =
  lambda_bindings_body 0 [] (fun arity tys _ _ -> (arity, tys)) (0, [])

and uncurry_lambda env is_rec base_arity var covar body =
  let arity', types' = get_lambda_bindings body in
  let covar =
    match uncurry_type (TyArrow (var, covar)) with
    | TyArrow (_, covar) -> covar
    | _ -> assert false
  in
  let arity = base_arity + arity' in
  if arity = 1 then
    T.(Lam (1, var, covar, _uncurry (T.Env.bind env (List.hd var)) body))
  else
    let types = var @ types' in
    if not is_rec then
      let env = List.fold_right (fun ty env -> T.Env.bind env ty) types env in
      let body = process_body env 1 1 types' body in
      T.(Lam (arity, types, covar, Let (List.hd var, tval (tvar 0), body)))
    else
      let env =
        List.fold_right
          (fun ty env -> T.Env.bind env ty)
          types
          (T.Env.bind env (TyArrow (types, covar)))
      in
      let body = process_body env 2 1 types' body in
      T.(
        Lam
          ( arity,
            types,
            covar,
            Let
              ( TyArrow (types, covar),
                tval (tvar arity),
                Let (List.hd var, tval (tvar 1), body) ) ))

and process_body env arg_pos depth types body =
  match body with
  | T.Let (ty, c, b) ->
      T.Let
        ( ty,
          uncurry_comp env false c,
          process_body (T.Env.bind env ty) arg_pos (succ depth) types b )
  | T.LetRec (ty, c, b) ->
      T.LetRec
        ( ty,
          uncurry_comp (T.Env.bind env ty) true c,
          process_body (T.Env.bind env ty) arg_pos (succ depth) types b )
  | T.(Computation (Lam (_, _, _, b))) -> fun_to_let env arg_pos depth types b
  | x -> _uncurry env x

and fun_to_let env arg_pos depth tys b =
  match tys with
  | [] -> b
  | [ ty ] ->
      T.(
        Let (ty, tval (tvar (arg_pos + depth)), _uncurry (T.Env.bind env ty) b))
  | ty :: tys ->
      T.(
        Let
          ( ty,
            tval (tvar (arg_pos + depth)),
            process_body (T.Env.bind env ty) (succ arg_pos) (succ depth) tys b
          ))

let uncurry = _uncurry T.Env.empty

(* let (int -> (int -> int) = fun int ->
     let int = var0 + 1 in
       fun int ->
         let int = var0 + 1 in
         let int = var3 + var2 in
         let int = var2 + var1 in
*)

let f =
  let open T in
  clet
    (arrow_ty int_ty (arrow_ty int_ty int_ty))
    (tlam 1 int_ty (arrow_ty int_ty int_ty)
       (clet int_ty
          (BinArith (tvar 0, Add, tint 1))
          (tcomp
             (tlam 1 int_ty int_ty
                (clet int_ty
                   (BinArith (tvar 0, Add, tint 1))
                   (clet int_ty
                      (BinArith (tvar 3, Add, tvar 2))
                      (clet int_ty
                         (BinArith (tvar 2, Add, tvar 1))
                         (tcomp (BinArith (tvar 1, Add, tvar 0))))))))))
    (tcomp (tval (tvar 0)))

let uncurried_f =
  let open T in
  clet
    (TyArrow ([ int_ty; int_ty ], int_ty))
    (Lam
       ( 2,
         [ int_ty; int_ty ],
         int_ty,
         clet int_ty
           (tval (tvar 0))
           (clet int_ty
              (BinArith (tvar 0, Add, tint 1))
              (clet int_ty
                 (tval (tvar 3))
                 (clet int_ty
                    (BinArith (tvar 0, Add, tint 1))
                    (clet int_ty
                       (BinArith (tvar 3, Add, tvar 2))
                       (clet int_ty
                          (BinArith (tvar 2, Add, tvar 1))
                          (tcomp (BinArith (tvar 1, Add, tvar 0)))))))) ))
    (tcomp (tval (tvar 0)))

let%test _ = uncurry f = uncurried_f

let f' =
  let open T in
  clet
    (arrow_ty int_ty (arrow_ty int_ty (arrow_ty int_ty int_ty)))
    (tlam 1 int_ty
       (arrow_ty int_ty (arrow_ty int_ty int_ty))
       (tcomp
          (tlam 1 int_ty (arrow_ty int_ty int_ty)
             (tcomp (tlam 1 int_ty int_ty (tcomp (tval (tint 42))))))))
    (tcomp (tval (tvar 0)))

let uncurried_f' =
  let open T in
  clet
    (TyArrow ([ int_ty; int_ty; int_ty ], int_ty))
    (Lam
       ( 3,
         [ TyInt; TyInt; TyInt ],
         int_ty,
         clet int_ty
           (tval (tvar 0))
           (clet int_ty
              (tval (tvar 2))
              (clet int_ty (tval (tvar 4)) (tcomp (tval (tint 42))))) ))
    (tcomp (tval (tvar 0)))

let%test _ = uncurry f' = uncurried_f'

let g =
  (*
   let ( int  -> (int -> (int -> int)) *  int) ) =
    fun (1 : int) ->
    (fun (2 : int * int) -> let var 0 in let var 2 in 0,
     42)

*)
  let open T in
  clet
    (arrow_ty int_ty
       (prod_ty (arrow_ty int_ty (arrow_ty int_ty int_ty)) int_ty))
    (tlam 1 int_ty
       (prod_ty (arrow_ty int_ty (arrow_ty int_ty int_ty)) int_ty)
       (clet
          (arrow_ty int_ty (arrow_ty int_ty int_ty))
          (tlam 1 int_ty (arrow_ty int_ty int_ty)
             (tcomp (tlam 1 int_ty int_ty (tcomp (tval (tint 0))))))
          (tcomp (Pair (tvar 0, tint 42)))))
    (tcomp (tval (tvar 0)))

let h =
  (*
 let (int -> (int -> int)) = fun (1 : int) -> fun (1 : int) -> var 1 + var 0 in
 let (int -> int) = var 0 41 in
 var 0 1
 *)
  let open T in
  clet
    (arrow_ty int_ty (arrow_ty int_ty int_ty))
    (tlam 1 int_ty (arrow_ty int_ty int_ty)
       (tcomp (tlam 1 int_ty int_ty (tcomp (BinArith (tvar 0, Add, tvar 1))))))
    (clet (arrow_ty int_ty int_ty)
       (tapp (tvar 0) (tint 41))
       (tcomp (tapp (tvar 0) (tint 1))))

let uncurried_h =
  let open T in
  clet
    (TyArrow ([ int_ty; int_ty ], int_ty))
    (Lam
       ( 2,
         [ TyInt; TyInt ],
         int_ty,
         clet TyInt
           (tval (tvar 0))
           (clet TyInt (tval (tvar 2)) (tcomp (BinArith (tvar 0, Add, tvar 1))))
       ))
    (clet (arrow_ty int_ty int_ty)
       (PartialApp (tval (tvar 0), [ tint 41 ]))
       (tcomp (tapp (tvar 0) (tint 1))))

let%test _ = uncurry h = uncurried_h
