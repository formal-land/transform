module S = Libcbpv.Syntax.Terms
module STypes = Libcbpv.Syntax.Types
module T = Libcbv.Syntax.Terms
module TTypes = Libcbv.Syntax.Types

type right
and left
and 'a t = Left : left t | Right : right t

let get_left : left t -> int = fun Left -> 42
and get_right : right t -> bool = fun Right -> true

let empty = []
let bind_val env x v = List.cons (x, v) env
let get env x = List.assoc x env

type env = (string * STypes.tyval S.term) list

let rec subst : type a. string -> STypes.tyval S.term -> a S.term -> a S.term =
 fun id v t ->
  match t with
  | S.Var x when id = x -> v
  | S.Fun (x, ty, c) when x <> id -> Fun (x, ty, subst id v c)
  | S.App (a, b) -> S.App (subst id v a, subst id v b)
  | S.Pair (a, b) -> S.Pair (subst id v a, subst id v b)
  | S.List ts -> S.List (List.map (subst id v) ts)
  | S.Let (x, ty, b, c) when x <> id -> S.Let (x, ty, subst id v b, subst id v c)
  | S.Let (x, ty, b, c) -> S.Let (x, ty, subst id v b, c)
  | S.To (b, x, ty, c) when x <> id -> S.To (subst id v b, x, ty, subst id v c)
  | S.To (b, x, ty, c) -> S.To (subst id v b, x, ty, c)
  | S.Thunk t -> S.Thunk (subst id v t)
  | S.Force t -> S.Force (subst id v t)
  | S.Return t -> S.Return (subst id v t)
  | S.Lit _ | S.Var _ | S.Fun _ -> t
  | _ -> assert false

let rec translate_ty : type a. a STypes.gtyp -> TTypes.ty =
 fun ty ->
  match ty with
  | STypes.TyUnit -> TTypes.TyUnit
  | STypes.TyInt -> TTypes.TyInt
  | STypes.TyBool -> TTypes.TyBool
  | STypes.TyArrow (a, b) -> TTypes.TyArrow ([ translate_ty a ], translate_ty b)
  | STypes.TyProd (a, b) -> TTypes.TyProd (translate_ty a, translate_ty b)
  | STypes.TyReturn a -> translate_ty a
  | STypes.TyThunk a -> TTypes.(TyArrow ([ TyUnit ], translate_ty a))
  | STypes.TyVar _ -> failwith "Types should be monomorphic"

let rec cbpv_val_to_cbv (v : STypes.tyval S.term) : T.term =
  match v with
  | S.Var x -> T.Var x
  | S.Lit (Int i) -> T.Lit (Int i)
  | S.Lit Unit -> T.Lit Unit
  | S.Pair (a, b) -> T.Pair (cbpv_val_to_cbv a, cbpv_val_to_cbv b)
  | S.Thunk c -> T.(Lam ("()", TTypes.TyUnit, cbpv_comp_to_cbv c))
  | S.List _ -> assert false
  | _ -> .

and cbpv_comp_to_cbv (t : STypes.tycomp S.term) : T.term =
  match t with
  | S.Fun (x, ty, b) -> T.Lam (x, translate_ty ty, cbpv_comp_to_cbv b)
  | S.App (a, b) -> T.App (cbpv_comp_to_cbv a, cbpv_val_to_cbv b)
  | S.Let (id, ty, (Thunk _ as b), c) ->
      let b = cbpv_val_to_cbv b in
      let c = cbpv_comp_to_cbv c in
      let ty = translate_ty ty in
      T.(Let (id, ty, b, c))
  | S.LetRec (id, ty, (Fun _ as b), c) ->
      let b = cbpv_val_to_cbv (Thunk b) in
      let c = cbpv_comp_to_cbv c in
      let ty = translate_ty (STypes.TyThunk ty) in
      T.(LetRec (id, ty, b, c))
  | S.LetRec _ -> failwith "Non functionnal recursive terms are unsupported"
  | S.Let (id, _, b, c) ->
      let c = subst id b c in
      cbpv_comp_to_cbv c
  | S.To (a, id, ty, b) ->
      T.Let (id, translate_ty ty, cbpv_comp_to_cbv a, cbpv_comp_to_cbv b)
  | S.Force a -> T.App (cbpv_val_to_cbv a, Lit Unit)
  | S.Return a -> cbpv_val_to_cbv a
  | S.If (c, bt, bf) ->
      T.If (cbpv_val_to_cbv c, cbpv_comp_to_cbv bt, cbpv_comp_to_cbv bf)
  | S.Prim (Fst t) -> T.Fst (cbpv_val_to_cbv t)
  | S.Prim (Snd t) -> T.Snd (cbpv_val_to_cbv t)
  | S.Prim (Plus (x, y)) ->
      T.BinArith (cbpv_val_to_cbv x, Add, cbpv_val_to_cbv y)
  | S.Prim (Minus (x, y)) ->
      T.BinArith (cbpv_val_to_cbv x, Minus, cbpv_val_to_cbv y)
  | S.Prim (Mult (x, y)) ->
      T.BinArith (cbpv_val_to_cbv x, Mult, cbpv_val_to_cbv y)
  | S.Prim (Cons (x, y)) -> T.Cons (cbpv_val_to_cbv x, cbpv_val_to_cbv y)
  | S.Prim (CompNZ x) -> T.CompNZ (cbpv_val_to_cbv x)
  | _ -> .

let%test _ =
  subst "one" (Lit (Int 1))
    S.(
      Let
        ( "x",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Return (Pair (Var "one", Lit (Int 42)))),
          Return (Var "x") ))
  = S.(
      Let
        ( "x",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Return (Pair (Lit (Int 1), Lit (Int 42)))),
          Return (Var "x") ))

let%test _ =
  subst "one" (Lit (Int 1))
    S.(
      Let
        ( "x",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Return (Pair (Lit (Int 42), Var "one"))),
          Return (Var "x") ))
  = S.(
      Let
        ( "x",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Return (Pair (Lit (Int 42), Lit (Int 1)))),
          Return (Var "x") ))

let%test _ =
  subst "one" (Lit (Int 1))
    S.(
      Let
        ( "x",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Fun ("y", STypes.TyInt, Return (Var "one"))),
          Return (Var "x") ))
  = S.(
      Let
        ( "x",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Fun ("y", STypes.TyInt, Return (Lit (Int 1)))),
          Return (Var "x") ))

let%test _ =
  subst "one" (Lit (Int 1))
    S.(
      Let
        ( "x",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Fun ("one", STypes.TyInt, Return (Var "one"))),
          Return (Var "x") ))
  = S.(
      Let
        ( "x",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Fun ("one", STypes.TyInt, Return (Var "one"))),
          Return (Var "x") ))

let%test _ =
  subst "one" (Lit (Int 1))
    S.(
      Let
        ( "one",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Fun ("y", STypes.TyInt, Return (Var "one"))),
          Return (Var "one") ))
  = S.(
      Let
        ( "one",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Fun ("y", STypes.TyInt, Return (Lit (Int 1)))),
          Return (Var "one") ))

let%test _ =
  subst "one" (Lit (Int 1))
    S.(
      Let
        ( "x",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Fun ("y", STypes.TyInt, Return (Var "one"))),
          Return (Var "one") ))
  = S.(
      Let
        ( "x",
          STypes.(TyThunk (TyArrow (TyInt, TyReturn TyInt))),
          Thunk (Fun ("y", STypes.TyInt, Return (Lit (Int 1)))),
          Return (Lit (Int 1)) ))
