let rec fact n = if n = 0 then 1 else n * fact (n - 1)

let%test _ = fact 5 = 120

let kid x = x
let kmul n k res = k (n * res)
let rec kfact n k = if n = 0 then k @@ 1 else kfact (pred n) @@ kmul n k

let%test _ = kfact 5 kid = 120

type (_, _) cont =
  | KId : ('a, 'a) cont
  | KMul : (int * ('a, 'a) cont) -> ('a, 'a) cont

let rec kdfact n k =
  if n = 0 then apply k 1 else (kdfact (pred n)) (KMul (n, k))

and apply k i = match k with KId -> i | KMul (n, k) -> apply k (n * i)

let%test _ = kdfact 5 KId = 120

let rec nunit n = if n = 0 then [] else () :: nunit (pred n)

let%test _ = nunit 5 = [ (); (); (); (); () ]

let kcons k (acc : 'a list) = k (() :: acc)
let u = ()
let rec knunit n k = if n = 0 then k @@ [] else knunit (pred n) @@ kcons k

let%test _ = knunit 5 kid = [ (); (); (); (); () ]

type (_, _) nunit_cont =
  | KId : ('a, 'a) nunit_cont
  | KCons :
      'a list * ('a list, 'a list) nunit_cont
      -> ('a list, 'a list) nunit_cont

and 'a step = Halt of 'a | Resume of (int * ('a, 'a) nunit_cont)

let rec kdnunit n k =
  if n = 0 then apply k [] else kdnunit (pred n) (KCons ([], k))

and apply k v =
  match k with KId -> () :: v | KCons (acc, k) -> apply k (() :: acc)

let rec tailnunit n k =
  match (n, k) with
  | n, KId -> tailnunit n (KCons ([], KId))
  | 0, KCons (acc, KId) -> acc
  | n, KCons (acc, k) -> tailnunit (pred n) (KCons (() :: acc, k))

let step = function
  | n, KId -> Resume (n, KCons ([], KId))
  | 0, KCons (acc, KId) -> Halt acc
  | n, KCons (acc, k) -> Resume (pred n, KCons (() :: acc, k))

let rec tailnunitb n k =
  match step (n, k) with Halt v -> v | Resume (n, k) -> tailnunitb n k

let%test _ = tailnunit 5 KId = [ (); (); (); (); () ]
