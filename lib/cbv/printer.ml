open PPrint
open Syntax
open Terms
open Values

let kw_fun = string "fun"
let kw_arrow = string "->"
let kw_unit_ty = string "unit"
let kw_int_ty = string "int"
let kw_bool_ty = string "bool"
let kw_list_ty = string "list"
let kw_star = star
let kw_let = string "let"
let kw_rec = string "rec"
let kw_letrec = group (prefix 0 1 kw_let kw_rec)
let kw_in = string "in"
let kw_compnz = string "=? 0"
let kw_fst = string "fst"
let kw_snd = string "snd"
let kw_if = string "if"
let kw_plus = plus
let kw_minus = minus
let kw_mult = star

let rec print_binder t =
  match t with
  | Let (x, ty, t1, t2) ->
      let var = string x in
      let ty = print_type ty in
      let t1 = print_binder t1 in
      let t2 = print_binder t2 in
      prefix 0 1
        (prefix 0 1 (group (kw_let ^^ space ^^ infix 0 1 colon var ty)) equals)
        (infix 0 1 kw_in (jump 2 1 t1) t2)
  | LetRec (x, ty, t1, t2) ->
      let var = string x in
      let ty = print_type ty in
      let t1 = print_binder t1 in
      let t2 = print_binder t2 in
      prefix 0 1
        (prefix 0 1
           (group (kw_letrec ^^ space ^^ infix 0 1 colon var ty))
           equals)
        (infix 0 1 kw_in (jump 2 1 t1) t2)
  | Lam (x, ty, t) ->
      let var = string x in
      let ty = print_type ty in
      let term = print_binder t in
      prefix 0 1 kw_fun
        (infix 0 1 kw_arrow
           (parens (infix 0 1 colon var ty))
           (group (jump 2 1 term)))
  | TypedTerm (t, ty) ->
      group (separate (string " : ") [ print_binder t; print_type ty ])
  | _ -> print_app t

and print_app t =
  match t with
  | App (t1, t2) -> prefix 2 1 (print_app t1) (print_atom t2)
  | BinArith (l, op, r) ->
      group (separate_map (print_op op) print_atom [ l; r ])
  | CompNZ a ->
      let a = print_atom a in
      prefix 0 1 a kw_compnz
  | Fst a ->
      let a = print_atom a in
      prefix 1 1 kw_fst a
  | Snd a ->
      let a = print_atom a in
      prefix 1 1 kw_snd a
  | If (c, tt, ff) ->
      let dc = print_term c in
      let dtt = print_term tt in
      let dff = print_term ff in
      prefix 0 1 kw_if
        (prefix 0 1
           (group (parens dc))
           (jump 1 0
              (group (group (braces dtt)) ^^ jump 0 1 (group (braces dff)))))
  | _ -> print_atom t

and print_op o =
  let op =
    match o with Add -> kw_plus | Minus -> kw_minus | Mult -> kw_mult
  in
  surround 0 1 space op space

and print_atom t =
  match t with
  | Var _ -> print_pattern t
  | Lit l -> print_litteral l
  | Pair (x, y) -> OCaml.tuple (List.map print_atom [ x; y ])
  | List [] -> OCaml.list (fun x -> x) []
  | List xs -> OCaml.list print_atom xs
  | _ -> group (parens (print_term t))

and print_litteral l = match l with Unit -> OCaml.unit | Int i -> OCaml.int i

and print_pattern t =
  match t with Var x -> string x | _ -> failwith "Printer: not a pattern"

and print_term x = print_binder x

and print_type_arrow t =
  let open Syntax.Types in
  match t with
  | TyArrow ([], _) -> assert false
  | TyArrow ([ t1 ], t2) ->
      infix 2 1 kw_arrow (print_type t1) (print_type_atom t2)
  | TyArrow (args_tys, t2) ->
      infix 2 1 kw_arrow
        (surround 2 0 lparen
           (separate_map kw_star print_type_atom args_tys)
           rparen)
        (print_type_atom t2)
  | _ -> print_type_atom t

and print_type_atom t =
  match t with
  | TyUnit -> kw_unit_ty
  | TyInt -> kw_int_ty
  | TyProd (ty1, ty2) ->
      surround 0 1 lparen
        (separate_map kw_star print_type_atom [ ty1; ty2 ])
        rparen
  | TyArrow _ -> parens (print_type t)
  | TyList ty -> prefix 0 1 (print_type_atom ty) kw_list_ty
  | TyBool -> kw_bool_ty
  | _ -> assert false

and print_type ty = group (print_type_arrow ty)

let rec print_value v =
  match v with
  | VUnit -> OCaml.unit
  | VInt i -> OCaml.int i
  | VBool b -> OCaml.bool b
  | VClo _ -> angles kw_fun
  | VList xs -> OCaml.list print_value xs
  | VPair (x, y) ->
      let vdocs = List.map print_value [ x; y ] in
      OCaml.tuple vdocs
