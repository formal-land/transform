open Syntax
open Terms
module T = Types

module S = struct
  type 'a structure =
    | TyInt
    | TyUnit
    | TyBool
    | TyArrow of ('a list * 'a)
    | TyProd of ('a * 'a)
    | TyList of 'a

  let map f s =
    match s with
    | TyArrow (t1, t2) -> TyArrow (List.map f t1, f t2)
    | TyProd (t1, t2) -> TyProd (f t1, f t2)
    | TyList ty -> TyList (f ty)
    | TyBool -> TyBool
    | TyUnit -> TyUnit
    | TyInt -> TyInt

  let fold f t acc =
    match t with
    | TyArrow (t1, t2) ->
        let acc = List.fold_right f t1 acc in
        f t2 acc
    | TyList ty -> f ty acc
    | TyProd (t1, t2) ->
        let acc = f t1 acc in
        f t2 acc
    | _ -> acc

  let iter f s = ignore (map f s)

  exception Iter2

  let iter2 f t u =
    match (t, u) with
    | TyArrow (t1, t2), TyArrow (u1, u2) ->
        (try List.iter2 f t1 u1 with Invalid_argument _ -> raise Iter2);
        f t2 u2
    | TyProd (t1, t2), TyProd (u1, u2) ->
        f t1 u1;
        f t2 u2
    | TyList t, TyList u -> f t u
    | TyBool, TyBool | TyUnit, TyUnit | TyInt, TyInt -> ()
    | _ -> raise Iter2
end

module O = struct
  type tyvar = int
  type 'a structure = 'a S.structure
  type ty = T.ty

  let solver_tyvar n = n
  let variable x = T.TyVar x

  let structure t =
    match t with
    | S.TyUnit -> T.TyUnit
    | S.TyBool -> T.TyBool
    | S.TyInt -> T.TyInt
    | S.TyList t -> T.TyList t
    | S.TyArrow (t1, t2) -> T.TyArrow (t1, t2)
    | S.TyProd (t1, t2) -> T.TyProd (t1, t2)

  let mu x t =
    ignore (x, t);
    assert false

  type scheme = tyvar list * ty
end

module Solver =
  Inferno.SolverHi.Make
    (struct
      include String

      type tevar = t
    end)
    (S)
    (O)

open Solver

let ty_unit = S.TyUnit
let ty_int = S.TyInt
let ty_arrow t1 t2 = S.TyArrow ([ t1 ], t2)
let ty_product t1 t2 = S.TyProd (t1, t2)
let ty_list t = S.TyList t

let rec convert_deep (ty : T.ty) =
  let conv = convert_deep in
  let deeps t = DeepStructure t in
  match ty with
  | TyVar _ -> failwith "types should be monomorph"
  | TyUnit -> deeps S.TyUnit
  | TyBool -> deeps S.TyBool
  | TyInt -> deeps S.TyInt
  | TyArrow (ty1, ty2) ->
      let ty1 = List.map conv ty1 in
      let ty2 = conv ty2 in
      deeps (S.TyArrow (ty1, ty2))
  | TyList ty -> deeps (S.TyList (conv ty))
  | TyProd (ty1, ty2) -> deeps (S.TyProd (conv ty1, conv ty2))

let convert ty =
  let deep_ty = convert_deep ty in
  build deep_ty

exception Poly of T.ty

let rec is_var (ty : T.ty) =
  match ty with
  | TyVar _ -> Some ty
  | TyInt | TyUnit | TyBool -> None
  | TyList t -> is_var t
  | TyArrow (x, y) ->
      List.fold_left
        (fun maybe_var ty -> match maybe_var with None -> is_var ty | t -> t)
        None (y :: x)
  | TyProd (ty1, ty2) -> ( match is_var ty1 with None -> is_var ty2 | t -> t)

let is_monomorphic ty = match is_var ty with Some _ -> false | _ -> true
let conj_of_term f cs t v = f t v ^& cs <$$> fun (x, xs) -> x :: xs
let conj_of_list f ts vs = List.fold_left2 (conj_of_term f) (pure []) ts vs

let rec hastype ty_env t w =
  match t with
  | Var x -> (
      try
        let ty = List.assoc x ty_env in
        convert ty (fun v -> w -- v) <$$> fun () -> TypedTerm (t, ty)
      with Not_found ->
        raise (Solver.Unbound ((Lexing.dummy_pos, Lexing.dummy_pos), x)))
  | Lam (x, ty, t) ->
      convert ty (fun v1 ->
          exist (fun v2 ->
              (w --- ty_arrow v1 v2)
              ^^ def x v1 (hastype ((x, ty) :: ty_env) t v2)))
      <$$> fun (ty2, u') ->
      let out_ty = T.TyArrow ([ ty ], ty2) in
      if is_monomorphic out_ty then TypedTerm (Lam (x, ty, u'), out_ty)
      else raise (Poly out_ty)
  | Lit Unit as k -> w --- ty_unit <$$> fun () -> TypedTerm (k, TyUnit)
  | Lit (Int _) as k -> w --- ty_int <$$> fun () -> TypedTerm (k, TyInt)
  | Pair (t1, t2) ->
      exist (fun v1 ->
          exist (fun v2 ->
              (w --- ty_product v1 v2)
              ^^ hastype ty_env t1 v1 ^& hastype ty_env t2 v2))
      <$$> fun (ty1, (ty2, (t1, t2))) ->
      TypedTerm (Pair (t1, t2), TyProd (ty1, ty2))
  | List xs ->
      exist (fun v ->
          (w --- ty_list v)
          ^^ List.fold_right
               (fun term co -> conj_of_term (hastype ty_env) co term v)
               xs (pure []))
      <$$> fun (t, xs) -> TypedTerm (List xs, TyList t)
  | CompNZ x ->
      exist_ (fun ty -> (w --- S.TyBool) ^^ hastype ty_env x ty) <$$> fun t ->
      TypedTerm (CompNZ t, TyBool)
  | Fst x ->
      exist (fun u ->
          exist_ (fun v -> (u -- w) ^^ lift (hastype ty_env) x (ty_product u v)))
      <$$> fun (ty, t) -> TypedTerm (Fst t, ty)
  | Snd x ->
      exist (fun u ->
          exist_ (fun v -> (u -- w) ^^ lift (hastype ty_env) x (ty_product v u)))
      <$$> fun (ty, t) -> TypedTerm (Snd t, ty)
  | If (c, tt, ff) ->
      exist (fun t ->
          (w -- t)
          ^^ lift (hastype ty_env) c TyBool
          ^& hastype ty_env tt w ^& hastype ty_env ff w)
      <$$> fun (t, (c, (tt, ff))) -> TypedTerm (If (c, tt, ff), t)
  | BinArith (t1, op, t2) ->
      exist_ (fun ty1 ->
          exist_ (fun ty2 ->
              (w --- S.TyInt) ^^ (w -- ty1) ^^ (w -- ty2)
              ^^ hastype ty_env t1 ty1 ^& hastype ty_env t2 ty2))
      <$$> fun (t1, t2) -> TypedTerm (BinArith (t1, op, t2), TyInt)
  | Cons (t1, t2) ->
      exist_ (fun ty1 ->
          exist (fun ty2 ->
              (w --- ty_list ty1)
              ^^ (w -- ty2) ^^ hastype ty_env t1 ty1 ^& hastype ty_env t2 ty2))
      <$$> fun (ty, (t1, t2)) -> TypedTerm (Cons (t1, t2), ty)
  | App (t1, t2) ->
      exist (fun aty ->
          exist_ (fun v ->
              (aty -- w)
              ^^ lift (hastype ty_env) t1 (ty_arrow v w)
              ^& hastype ty_env t2 v))
      <$$> fun (ty, (t1, t2)) -> TypedTerm (App (t1, t2), ty)
  | Let (x, ty, t1, t2) -> (
      convert ty (fun ty1 ->
          hastype ty_env t1 ty1 ^& def x ty1 (hastype ((x, ty) :: ty_env) t2 w))
      <$$> function
      | (TypedTerm (_, ty1) as t1), (TypedTerm (_, ty2) as t2) ->
          TypedTerm (Let (x, ty1, t1, t2), ty2)
      | _ -> assert false)
  | LetRec (x, ty, t1, t2) -> (
      let env = (x, ty) :: ty_env in
      convert ty (fun ty1 -> def x ty1 (hastype env t1 ty1 ^& hastype env t2 w))
      <$$> function
      | (TypedTerm (_, ty1) as t1), (TypedTerm (_, ty2) as t2) ->
          TypedTerm (LetRec (x, ty1, t1, t2), ty2)
      | _ -> assert false)
  | TypedTerm (t, ty) ->
      convert ty (fun v -> (w -- v) ^^ hastype ty_env t w) <$$> fun t -> t

exception Unbound = Solver.Unbound
exception Unify = Solver.Unify
exception Cycle = Solver.Cycle

let elaborate t = solve false (let0 (exist_ (hastype [] t)) <$$> fun (_, t) -> t)
