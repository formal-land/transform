%{ (* Emacs, use -*- tuareg -*- to open this file. *)
   module Libcbv = struct end
   open Syntax.Terms


%}

%token EOF
%token FUN LET REC IN INT UNIT FST SND COMPNZ IF THEN ELSE
%token ARROW LPAREN RPAREN COLON CONS EQUAL COMMA LIST
%token TIMES MINUS PLUS
%token<string> ID
%token<int> LINT
%token LUNIT
%token NIL

%start<Syntax.Terms.program> program
%right ARROW IN CONS
%nonassoc ELSE
%left PLUS MINUS
%left TIMES

%%
program: p=term EOF { p }

term:
a=abstraction
{
  a
}
| d=definition IN t=term
{
  Let(fst (fst d),snd (fst d),snd d, t)
}
| d=rec_definition IN t=term
{
  LetRec(fst (fst d),snd (fst d),snd d, t)
}
| lhs=term CONS rhs=term
{
  Cons (lhs,rhs)
}
| lhs=term b=binop rhs=term
{
  BinArith (lhs, b, rhs)
}
| IF c=term THEN bt=term ELSE bf=term
{
  If(c,bt,bf)
}
| t=simple_term
{
  t
}

simple_term:
  a=simple_term b=atomic_term
{
  App (a, b)
}
| FST a=atomic_term
{
  Fst a
}
| SND a=atomic_term
{
  Snd a
}
| COMPNZ a=atomic_term
{
  CompNZ a
}
| a=atomic_term
{
  a
}

atomic_term:
x=ID {
  Var x
    }
| NIL
{ List [] }
| l=literal {
  Lit l
}
| LPAREN lsh=term COMMA rhs=term RPAREN {
  Pair(lsh,rhs)
}
| LPAREN t = term RPAREN { t }

literal:
  i=LINT
{
  Int i
}
| LUNIT
{
  Unit
}

%inline abstraction: FUN bs=binding+ ARROW t=term
{
  make_lambda_abstraction bs t
}

%inline definition: LET b=binding EQUAL t=term
{
  (b, t)
}
| LET x=ID arg=binding
  COLON oty=typ
  EQUAL t=term
{
  let tyf =  Syntax.Types.TyArrow ([snd arg], oty) in
  ((x, tyf), Syntax.Terms.make_lambda_abstraction [arg] t)
}

%inline rec_definition: LET REC b=binding EQUAL t=term
{
  (b, t)
}
| LET REC x=ID arg=binding
  COLON oty=typ
  EQUAL t=term
{
  let tyf =  Syntax.Types.TyArrow ([snd arg], oty) in
  ((x, tyf), Syntax.Terms.make_lambda_abstraction [arg] t)
}

%inline binop :
| PLUS  { Add }
| TIMES { Mult }
| MINUS { Minus }

binding: LPAREN x=ID COLON ty=typ RPAREN
{
  (x, ty)
}

typ: x=type_constant
{
  x
}
| lhs=type_constant ARROW rhs=typ
{
  Syntax.Types.TyArrow ([lhs], rhs)
}
| lhs=type_constant TIMES rhs=typ
{
  Syntax.Types.TyProd (lhs, rhs)
}
| t=type_constant LIST
{ Syntax.Types.TyList t}

type_constant:
  INT
{
  Syntax.Types.TyInt
}
| UNIT
{
  Syntax.Types.TyUnit
}
| LPAREN t=typ RPAREN
{
  t
}
