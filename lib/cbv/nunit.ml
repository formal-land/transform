type cont = KId | KCons of cont

let rec dcnunint n k = if n = 0 then apply k [] else dcnunint (pred n) (KCons k)
and apply k acc = match k with KId -> acc | KCons k -> apply k (() :: acc)

let%test _ = dcnunint 5 KId = List.init 5 (fun _ -> ())

let rec tnunint n k s =
  match (n, k, s) with
  | 0, 0, Some s -> s
  | 0, 0, None -> []
  | 0, k, Some acc -> tnunint 0 (pred k) (Some (() :: acc))
  | 0, k, None -> tnunint 0 k (Some [])
  | n, k, s -> tnunint (pred n) (succ k) s

let%test _ = tnunint 6 0 None = List.init 6 (fun _ -> ())
