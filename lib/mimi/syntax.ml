module Instructions = struct
  type (_, _) instr =
    | Push : 'value -> ('stack, 'value * 'stack) instr
    | Add : (int * (int * 'stack), int * 'stack) instr
    | Cons : ('stack, 't) instr * ('t, 'u) instr -> ('stack, 'u) instr

  let push i = Push i
  let add = Add
  let ( @ ) i1 i2 = Cons (i1, i2)
  let push_1_push_2_add : (unit, int * unit) instr = push 1 @ push 2 @ add
end

module Types = struct
  type ty =
    | TyInt
    | TyUnit
    | TyBool
    | TyProd of ty * ty
    | TyArrow of ty * ty
    | TyList of ty
end

module Terms = struct
  open Types

  type node = Prim of primitive | Seq of (node * node) | Lit of value
  and value = Int of int | Unit | List of value list

  and primitive =
    | Add
    | Minus
    | Mult
    | CompNZ
    | Push of (ty * value)
    | Lam of (ty * ty * node)
    | LamRec of (ty * ty * node)
    | Pair
    | Unpair
    | Fst
    | Snd
    | Exec
    | Fix
    | Apply
    | If of (node * node)
    | Dup of int
    | Dig of int
    | Drop of int
    | Dip of node
    | Cons
    | Swap
    | Nil of ty

  and term = node
end

module Cont = struct
  type (_, _, _, _) cont =
    | KNil : ('top, 'stack, 'top, 'stack) cont
    | KCons :
        ('itop, 'input, 'ktop, 'k) cinstr * ('ktop, 'k, 'otop, 'output) cont
        -> ('itop, 'input, 'otop, 'output) cont
    | KReturn :
        'input * ('itop, 'input, 'otop, 'output) cont
        -> ('itop, end_of_stack, 'otop, 'output) cont

  and end_of_stack = unit * unit

  and (_, _, _, _) cinstr =
    | IHalt : ('top, 'stack, 'top, 'stack) cinstr
    | IAdd :
        (int, 'input, 'otop, 'output) cinstr
        -> (int, int * 'input, 'otop, 'output) cinstr
    | IMinus :
        (int, 'input, 'otop, 'output) cinstr
        -> (int, int * 'input, 'otop, 'output) cinstr
    | IMult :
        (int, 'input, 'otop, 'output) cinstr
        -> (int, int * 'input, 'otop, 'output) cinstr
    | ICompNZ :
        (bool, 'input, 'otop, 'output) cinstr
        -> (int, 'input, 'otop, 'output) cinstr
    | IPush :
        'value * ('value, 'itop * 'input, 'otop, 'output) cinstr
        -> ('itop, 'input, 'otop, 'output) cinstr
    | ILambda :
        ('arg, 'ret) lambda
        * (('arg, 'ret) lambda, 'itop * 'input, 'otop, 'output) cinstr
        -> ('itop, 'input, 'otop, 'output) cinstr
    | IIf :
        ('itop, 'input, 'otop, 'output) cinstr
        * ('itop, 'input, 'otop, 'output) cinstr
        -> (bool, 'itop * 'input, 'otop, 'output) cinstr
    | IApp :
        (('a2, 'ret) lambda, 'input, 'otop, 'output) cinstr
        -> (('a1 * 'a2, 'ret) lambda, 'a1 * 'input, 'otop, 'output) cinstr
    | IFix :
        (('arg, 'ret) lambda, 'input, 'otop, 'output) cinstr
        -> ( (('arg, 'ret) lambda * 'arg, 'ret) lambda,
             'input,
             'otop,
             'output )
           cinstr
    | IExec :
        ('ret, 'input, 'otop, 'output) cinstr
        -> (('arg, 'ret) lambda, 'arg * 'input, 'otop, 'output) cinstr
    | IPair :
        ('fst * 'snd, 'input, 'otop, 'output) cinstr
        -> ('fst, 'snd * 'input, 'otop, 'output) cinstr
    | IUnpair :
        ('fst, 'snd * 'input, 'otop, 'output) cinstr
        -> ('fst * 'snd, 'input, 'otop, 'output) cinstr
    | IFst :
        ('fst, 'input, 'otop, 'output) cinstr
        -> ('fst * 'snd, 'input, 'otop, 'output) cinstr
    | ISnd :
        ('snd, 'input, 'otop, 'output) cinstr
        -> ('fst * 'snd, 'input, 'otop, 'output) cinstr
    | IDig :
        ('itop, 'input, 'ktop, 'k, 'elt, 'ts * 'suffix, 'ts, 'suffix) proof
        * ('elt, 'ktop * 'k, 'otop, 'output) cinstr
        -> ('itop, 'input, 'otop, 'output) cinstr
    | IDup :
        ('itop * 'input, 'elt) dup_n_gadt_witness
        * ('elt, 'itop * 'input, 'otop, 'output) cinstr
        -> ('itop, 'input, 'otop, 'output) cinstr
    | IDrop :
        ( 'itop,
          'input,
          'itop,
          'input,
          'suffixtop,
          'suffix,
          'suffixtop,
          'suffix )
        proof
        * ('suffixtop, 'suffix, 'otop, 'output) cinstr
        -> ('itop, 'input, 'otop, 'output) cinstr
    | IDip :
        ('isnd, 'input, 'ktop, 'k) cinstr
        * ('itop, 'ktop * 'k, 'otop, 'output) cinstr
        -> ('itop, 'isnd * 'input, 'otop, 'output) cinstr
    | ICons :
        ('a list, 'input, 'otop, 'output) cinstr
        -> ('a, 'a list * 'input, 'otop, 'output) cinstr
    | ISwap :
        ('b, 'a * 'input, 'otop, 'output) cinstr
        -> ('a, 'b * 'input, 'otop, 'output) cinstr
    | INil :
        ('a list, 'itop * 'input, 'otop, 'output) cinstr
        -> ('itop, 'input, 'otop, 'output) cinstr

  and ('arg, 'ret) lambda =
    | Lam :
        ('arg ty * 'ret ty * ('arg, end_of_stack, 'ret, end_of_stack) cinstr)
        -> ('arg, 'ret) lambda
    | LamRec :
        ('arg ty
        * 'ret ty
        * ('arg, ('arg, 'ret) lambda * end_of_stack, 'ret, end_of_stack) cinstr)
        -> ('arg, 'ret) lambda

  and ('tti,
        'toplevel_input_stack,
        'tto,
        'toplevel_output_stack,
        'tsi,
        'suffix_input_stack,
        'tso,
        'suffix_output_stack)
      proof =
    | KStack : (* prefix is empty. *)
        ('ts, 's, 'tu, 'u, 'ts, 's, 'tu, 'u) proof
    | KPrefix :
        ('ts, 's, 'tt, 't, 'tv, 'v, 'tw, 'w) proof
        -> ('a, 'ts * 's, 'a, 'tt * 't, 'tv, 'v, 'tw, 'w) proof

  and (_, _) dup_n_gadt_witness =
    | Dup_n_zero : ('a * 'rest, 'a) dup_n_gadt_witness
    | Dup_n_succ :
        ('stack, 'b) dup_n_gadt_witness
        -> ('a * 'stack, 'b) dup_n_gadt_witness

  and _ ty =
    | TyUnit : unit ty
    | TyInt : int ty
    | TyBool : bool ty
    | TyLam : ('arg ty * 'ret ty) -> ('arg, 'ret) lambda ty
    | TyProd : ('fst ty * 'snd ty) -> ('fst * 'snd) ty
    | TyList : 'a ty -> 'a list ty
end
