open PPrint
open Syntax
open DeBruijnTerms

let kw_fun = string "fun"
let kw_arrow = string "->"
let kw_let = string "let"
let kw_in = string "in"
let kw_fst = string "fst"
let kw_snd = string "snd"
let kw_if = string "if"
let kw_compnz = string "0 =?"
let kw_var = string "var"
let kw_plus = plus
let kw_minus = minus
let kw_mult = star
let kw_at = at
let kw_unit_ty = string "unit"
let kw_int_ty = string "int"
let kw_bool_ty = string "bool"
let kw_star = star
let kw_rec = string "rec"
let kw_letrec = group (prefix 0 1 kw_let kw_rec)

let rec print_body b =
  match b with
  | Let (ty, c, b) ->
      let ty = print_type ty in
      let c = print_computation c in
      let b = print_body b in
      group
        (kw_let ^^ space ^^ ty ^^ space ^^ equals ^^ space
       ^^ infix 0 1 kw_in c b)
  | LetRec (ty, c, b) ->
      let ty = print_type ty in
      let c = print_computation c in
      let b = print_body b in
      group
        (kw_letrec ^^ space ^^ ty ^^ space ^^ equals ^^ space
       ^^ infix 0 1 kw_in c b)
  | Computation c -> print_computation c

and print_computation c =
  match c with
  | Val v -> print_atom v
  | Lam (arity, ty, _, b) ->
      let ty = separate_map kw_star print_type ty in
      parens
        (prefix 1 1
           (kw_fun ^^ space
           ^^ parens (OCaml.int arity ^^ colon ^^ ty)
           ^^ space ^^ kw_arrow)
           (print_body b))
  | App (a, b) ->
      let a = print_atom a in
      let b = print_atom b in
      prefix 1 1 a b
  | Fst a ->
      let a = print_atom a in
      prefix 1 1 kw_fst a
  | Snd a ->
      let a = print_atom a in
      prefix 1 1 kw_snd a
  | CompNZ a ->
      let a = print_atom a in
      prefix 1 1 kw_compnz a
  | BinArith (l, op, r) ->
      group (separate_map (print_op op) print_atom [ l; r ])
  | If (c, tt, ff) ->
      let dc = print_atom c in
      let dtt = print_body tt in
      let dff = print_body ff in
      prefix 0 1 kw_if
        (prefix 0 1
           (group (parens dc))
           (jump 1 0
              (group (group (braces dtt)) ^^ jump 0 1 (group (braces dff)))))
  | PartialApp (a, ps) ->
      let ps = separate_map kw_at print_atom ps in
      infix 1 1 kw_at (print_computation a) ps
  | Pair (f, s) -> parens (separate_map comma print_atom [ f; s ])
  | Cons (f, s) -> separate_map (twice colon) print_atom [ f; s ]
  | List (_, xs) -> OCaml.list print_atom xs

and print_atom a =
  match a with Var i -> kw_var ^^ OCaml.int i | Lit l -> print_lit l

and print_lit l = match l with Int i -> OCaml.int i | Unit -> OCaml.unit

and print_op o =
  match o with Add -> kw_plus | Minus -> kw_minus | Mult -> kw_mult

and print_type_arrow t =
  let open Syntax.Types in
  match t with
  | TyArrow ([], _) -> assert false
  | TyArrow ([ t1 ], t2) ->
      infix 2 1 kw_arrow (print_type t1) (print_type_atom t2)
  | TyArrow (args_tys, t2) ->
      infix 2 1 kw_arrow
        (surround 2 0 lparen
           (separate_map kw_star print_type_atom args_tys)
           rparen)
        (print_type_atom t2)
  | _ -> print_type_atom t

and print_type_atom t =
  match t with
  | TyUnit -> kw_unit_ty
  | TyInt -> kw_int_ty
  | TyBool -> kw_bool_ty
  | TyProd (ty1, ty2) ->
      surround 0 1 lparen
        (separate_map kw_star print_type_atom [ ty1; ty2 ])
        rparen
  | TyArrow _ -> parens (print_type t)
  | _ -> assert false

and print_type ty = group (print_type_arrow ty)

let rec print_value v =
  let open Values in
  match v with
  | VUnit -> OCaml.unit
  | VInt i -> OCaml.int i
  | VBool i -> OCaml.bool i
  | VClo _ -> angles kw_fun
  | VPair (x, y) ->
      let vdocs = List.map print_value [ x; y ] in
      OCaml.tuple vdocs
  | VList xs -> OCaml.list print_value xs
