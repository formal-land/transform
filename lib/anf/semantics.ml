open Syntax

module BigStep = struct
  open Terms
  open Env
  open Values

  let rec eval env t =
    match t with
    | Let (v, _, t, b) ->
        let env = bind env v (compute env t) in
        eval env b
    | LetRec (v, _, t, b) ->
        let env = bind env v VUnit in
        let vx = compute env t in
        let env = update env v vx in
        eval env b
    | Computation c -> compute env c

  and compute env c =
    match c with
    | Val v -> closed_value env v
    | Lam (xs, _, _, t) -> VClo (xs, t, env)
    | App (a, b) ->
        let b = closed_value env b in
        let a =
          match closed_value env a with
          | VClo (x, t, env) -> eval (bind env x b) t
          | _ -> assert false
        in
        a
    | BinArith (la, Add, ra) -> (
        match (closed_value env la, closed_value env ra) with
        | VInt la, VInt ra -> VInt (la + ra)
        | _ -> assert false)
    | BinArith (la, Minus, ra) -> (
        match (closed_value env la, closed_value env ra) with
        | VInt la, VInt ra -> VInt (la - ra)
        | _ -> assert false)
    | BinArith (la, Mult, ra) -> (
        match (closed_value env la, closed_value env ra) with
        | VInt la, VInt ra -> VInt (la * ra)
        | _ -> assert false)
    | If (c, bt, bf) -> (
        match closed_value env c with
        | VBool true -> eval env bt
        | VBool false -> eval env bf
        | _ -> assert false)
    | Pair (f, s) -> VPair (closed_value env f, closed_value env s)
    | Cons (x, xs) ->
        let x = closed_value env x in
        let xs =
          match closed_value env xs with VList xs -> xs | _ -> assert false
        in
        VList (x :: xs)
    | List (_, xs) -> VList (List.map (closed_value env) xs)
    | Fst x -> (
        match closed_value env x with VPair (x, _) -> x | _ -> assert false)
    | Snd x -> (
        match closed_value env x with VPair (_, x) -> x | _ -> assert false)
    | CompNZ x -> (
        match closed_value env x with
        | VInt 0 -> VBool true
        | VInt _ -> VBool false
        | _ -> assert false)

  and closed_value env x =
    match x with
    | Var x -> get env x
    | Lit Unit -> VUnit
    | Lit (Int i) -> VInt i

  let value_of = eval empty

  let%test _ = value_of cunit = vunit
  (* let%test _ = value_of capp_id_id = tfun_id *)
  let%test _ = value_of capp_id_idy_unit = vunit
  (* let%test _ = value_of one_minus_42 = vint (-41)
   * let%test _ = value_of succ_42 = vint 43 *)
  let%test _ = value_of capture_test = vint 42

  let%test _ =
    value_of
      (Let
         ( "x",
           TyBool,
           CompNZ (Lit (Int 0)),
           Computation
             (If
                ( Var "x",
                  Computation (Val (Lit (Int 42))),
                  Computation (Val (Lit (Int 41))) )) ))
    = vint 42

  let%test _ = value_of (fact 5) = vint 120
end

module DeBruijnBigStep = struct
  open DeBruijnTerms
  open Env
  open Values

  let print_env e =
    let open PPrint in
    let doc_of_env = separate_map semi (fun v -> Printer.print_value !v) in
    Printf.printf "ENV : %s\n" (Libcbv.IO.document_to_string doc_of_env e)

  let rec eval env t =
    match t with
    | Let (_, t, b) ->
        let env = bind env (compute env t) in
        eval env b
    | LetRec (_, Lam (_, _, _, lb), b) ->
        let env' = bind env VUnit in
        let vx = VClo (lb, env', []) in
        update env' 0 vx;
        eval env' b
    | Computation c -> compute env c
    | _ -> failwith "Recursive values are unsupported"

  and compute env c =
    match c with
    | Val v -> closed_value env v
    | Lam (_, _, _, b) -> VClo (b, env, [])
    | App (a, b) ->
        let vb = closed_value env b in
        let a =
          match closed_value env a with
          | VClo (a, env', p) ->
              let env' = bind env' vb in
              let env' = List.fold_left (fun e a -> bind e a) env' p in
              eval env' a
          | _ -> assert false
        in
        a
    | BinArith (la, Add, ra) -> (
        match (closed_value env la, closed_value env ra) with
        | VInt la, VInt ra -> VInt (la + ra)
        | _ -> assert false)
    | BinArith (la, Minus, ra) -> (
        match (closed_value env la, closed_value env ra) with
        | VInt la, VInt ra -> VInt (la - ra)
        | _ -> assert false)
    | BinArith (la, Mult, ra) -> (
        match (closed_value env la, closed_value env ra) with
        | VInt la, VInt ra -> VInt (la * ra)
        | _ -> assert false)
    | If (c, bt, bf) -> (
        match closed_value env c with
        | VBool true -> eval env bt
        | VBool false -> eval env bf
        | _ -> assert false)
    | Fst x -> (
        match closed_value env x with VPair (x, _) -> x | _ -> assert false)
    | Snd x -> (
        match closed_value env x with VPair (_, x) -> x | _ -> assert false)
    | CompNZ x -> (
        match closed_value env x with
        | VInt 0 -> VBool true
        | VInt _ -> VBool false
        | _ -> assert false)
    | PartialApp (a, ps) ->
        let vps = List.map (closed_value env) ps in
        let a =
          match compute env a with
          | VClo (a, env', p) -> VClo (a, env', p @ vps)
          | _ -> assert false
        in
        a
    | Pair (f, s) -> VPair (closed_value env f, closed_value env s)
    | Cons (x, xs) ->
        let x = closed_value env x in
        let xs =
          match closed_value env xs with VList xs -> xs | _ -> assert false
        in
        VList (x :: xs)
    | List (_, xs) -> VList (List.map (closed_value env) xs)

  and closed_value env x =
    match x with
    | Var x -> get env x
    | Lit (Int i) -> VInt i
    | Lit Unit -> VUnit

  let value_of t = eval empty t

  let%test _ = value_of test_id = VInt 1
  let%test _ = value_of capp_id_one = VInt 1
  let%test _ = value_of t = VInt 42

  let%test _ =
    value_of
      (Let
         ( TyBool,
           CompNZ (Lit (Int 0)),
           Computation
             (If
                ( Var 0,
                  Computation (Val (Lit (Int 42))),
                  Computation (Val (Lit (Int 41))) )) ))
    = VInt 42

  let%test _ = value_of (fact 6) = VInt 720
end
