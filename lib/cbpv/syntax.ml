module Types = struct
  type ('a, _) typ =
    | TyUnit : ('a, tyval) typ
    | TyInt : ('a, tyval) typ
    | TyBool : ('a, tyval) typ
    | TyVar : 'a -> ('a, tyval) typ
    | TyArrow : ('a, tyval) typ * ('a, tycomp) typ -> ('a, tycomp) typ
    | TyProd : ('a, tyval) typ * ('a, tyval) typ -> ('a, tyval) typ
    | TyReturn : ('a, tyval) typ -> ('a, tycomp) typ
    | TyThunk : ('a, tycomp) typ -> ('a, tyval) typ

  and 'a gtyp = (int, 'a) typ
  and tycomp = TyComp
  and tyval = TyVal
end

module Terms = struct
  open Types

  type _ term =
    | Lit : lit -> tyval term
    | Var : string -> tyval term
    | Fun : string * tyval gtyp * tycomp term -> tycomp term
    | App : tycomp term * tyval term -> tycomp term
    | Pair : tyval term * tyval term -> tyval term
    | List : tyval term list -> tyval term
    | Let : string * tyval gtyp * tyval term * tycomp term -> tycomp term
    | LetRec : string * tycomp gtyp * tycomp term * tycomp term -> tycomp term
    | To : tycomp term * string * tyval gtyp * tycomp term -> tycomp term
    | Force : tyval term -> tycomp term
    | Thunk : tycomp term -> tyval term
    | Return : tyval term -> tycomp term
    | If : tyval term * tycomp term * tycomp term -> tycomp term
    | Prim : 'a prim -> 'a term

  and lit = Unit | Int of int

  and _ prim =
    | Fst : tyval term -> tycomp prim
    | Snd : tyval term -> tycomp prim
    | Plus : tyval term * tyval term -> tycomp prim
    | Minus : tyval term * tyval term -> tycomp prim
    | Mult : tyval term * tyval term -> tycomp prim
    | Cons : tyval term * tyval term -> tycomp prim
    | CompNZ : tyval term -> tycomp prim

  and _ terminal =
    | VUnit : tyval terminal
    | VInt : int -> tyval terminal
    | VPair : tyval terminal * tyval terminal -> tyval terminal
    | VList : tyval terminal list -> tyval terminal
    | VThunk : tycomp term * env -> tyval terminal
    | VForce : tyval terminal -> tycomp terminal
    | VFun : string * tycomp term * env -> tycomp terminal
    | VReturn : tyval terminal -> tycomp terminal

  and env = (string * tyval terminal ref) list
  and context = { time : int; env : env }

  let vint x = VInt x
  let vunit = VUnit
  let vpair x y = VPair (x, y)

  let vcons (x : tyval terminal) (y : tyval terminal) : tyval terminal =
    match y with VList xs -> VList (x :: xs) | _ -> assert false

  let ty_fun a b = TyArrow (a, TyReturn b)
  let ty_int = TyInt
  let ty_bool = TyBool
  let ty_thunk t = TyThunk t
  let ty_f t = TyReturn t
  let tfst x = Prim (Fst x)
  let tsnd x = Prim (Snd x)
  let tlet x ty t u = Let (x, ty, t, u)
  let tlet_rec x ty t u = LetRec (x, ty, t, u)
  let ( =: ) t (x, ty) u = To (t, x, ty, u)
  let ( |>! ) u t = App (t, u)
  let ( @@! ) t u = App (t, u)
  let tthunk t = Thunk t
  let tfun x ty t = Fun (x, ty, t)
  let tforce t = Force t
  let ( !! ) = tforce
  let treturn t = Return t
  let t x = Var x
  let ( +! ) x t = Prim (Plus (x, t))
  let ( *! ) x t = Prim (Mult (x, t))
  let ( -! ) x t = Prim (Minus (x, t))
  let tunit = Lit Unit
  let tint x = Lit (Int x)
  let tprod x y = Pair (x, y)
  let tcons x y = Prim (Cons (x, y))
  let tif c x y = If (c, x, y)
  let tfunn xs t = List.fold_right (fun (x, ty) t -> tfun x ty t) xs t
end
